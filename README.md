# Fantasy shop generator

## Project roadmap:

### Generate shop based on DM inputs </br>

[X] Design procedural generation rules and constraints</br>
[] Generate shop based on settlement size (GM input)</br>
[] Generate random shop name</br>

### Make the app a Rest API to use with a front-web framework </br>

[] Make the API using Flask</br>
[] Add a generated shop to a database</br>
[] Make the front-end interface</br>

### Make the project a stand-alone app with tauri </br>

[] Bundle API, DB and front-end together</br>
[] Make it work (magic)</br>
