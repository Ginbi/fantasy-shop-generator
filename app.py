import time
from shop import Shop

def generate_shop():
    valid_size = False
    location_size = input('How big is this location ?\n(1) Hamlet\n(2) Village\n(3) Major city\n')
    while valid_size != True:
        if location_size == '1' or location_size == 'Hamlet':
            small_shop = Shop('small')
            print(small_shop.name)
            valid_size = True
        elif location_size == '2' or location_size == 'Village':
            medium_shop = Shop('medium')
            print(medium_shop.name)
            valid_size = True
        elif location_size == '3' or location_size == 'Major city':
            big_shop = Shop('big')
            print(big_shop.name)
            valid_size = True
        else :
            print('Incorrect input value, try again')
            time.sleep(1.5)
            location_size = input('How big is this location ?\n(1) Hamlet\n(2) Village\n(3) Major city\n')

generate_shop()

