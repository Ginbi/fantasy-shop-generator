class Shop:
       
    def __init__(self, size):
        # mettre un enum là-dedans
        if size == 'small':
            self.name = 'le ptit bar à dédé'
            self.items = [
                        {
                            'name': 'popo de soin',
                            'price': 4
                        },
                        {
                            'name': 'cure-dent',
                            'price': 7
                         }
                    ]
        elif size == 'medium':
            self.name = 'le bar à michou'
            self.items = [
                        {
                            'name': 'popo de mana',
                            'price': 4
                        },
                        {
                            'name': 'sabre',
                            'price': 7
                        }
                    ]
        elif size == 'big':
            self.name = 'le gros bar à jacky'
            self.items = [
                        {
                            'name': 'popo de feu',
                            'price': 4
                        },
                        {
                            'name': 'grossépé',
                            'price': 7
                        }
                    ]

